class Character {
    constructor(name, side, unit, rank, role, description, img) {
        this.name = name;
        this.side = side;
        this.unit = unit;
        this.rank = rank;
        this.role = role;
        this.description = description;
        this.img = img;
    }
}

const characters = [
    new Character(
        'Claude Wallace',
        'Edinburgh Army',
        'Ranger Corps, Squad E',
        'First Lieutenant',
        'Tank Commander',
        'Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.',
        'http://valkyria.sega.com/img/character/img-chara01.png'
    ),
    new Character(
        'Riley Miller',
        'Edinburgh Army',
        'Federate Joint Ops',
        'Second Lieutenant',
        'Artillery Advisor',
        'Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.',
        'http://valkyria.sega.com/img/character/img-chara02.png'
    ),
    new Character(
        'Raz',
        'Edinburgh Army',
        'Ranger Corps, Squad E',
        'Sergeant',
        'Fireteam Leader',
        'Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he\'s invincible.',
        'http://valkyria.sega.com/img/character/img-chara03.png'
    ),
    new Character(
        'Marie Bennett',
        'Edinburgh Navy',
        'Centurion, Cygnus Fleet',
        'Petty Officer',
        'Chief of Operations',
        'As the Centurion\'s crewmember responsible for overseeing daily operations, this gentle and supportive EWI veteran even takes daily tasks like cooking and cleaning upon herself. She never forgets to wear a smile. Her age is undisclosed, even in her personnel files.',
        'http://valkyria.sega.com/img/character/img-chara20.png'
    ),
    new Character(
       'Ragnarok',
       'Edinburgh Army',
       'Squad E',
       'K-9 Unit',
       'Mascot',
       'Once a stray, this good good boy is lovingly referred to as "Rags."As a K-9 unit, he\'s a brave and intelligent rescue dog who\'s always willing to lend a helping paw. When the going gets tough, the tough get ruff.',
       'http://valkyria.sega.com/img/character/img-chara13.png'
    ),
    new Character(
        'Crymaria Levin',
        'Imperial Army',
        'X-0, Ausbruch',
        'Special Agent',
        'Winter Witch',
        'Feared by both the Federation and the Empire as the legendary "Winter Witch," this unstable Valkyria was a laboratory test subject until being recruited by X-0. She is accompanied by a wolf named Fenrir, who she rescued from unethical experimentation.',
        'http://valkyria.sega.com/img/character/img-chara05.png'
    ),
    new Character(
        'Klaus Walz',
        'Imperial Army',
        'X-0, Ausbruch',
        'Lieutenant Colonel',
        'Tank Commander',
        'As leader of Ausbruch, the 502nd heavy tank platoon, this hedonistic yet dangerous commander is a force to be reckoned with. His faith in instincts over logic makes him as much a nightmare to his Federate opponents as he is a headache to the Imperial military.',
        'http://valkyria.sega.com/img/character/img-chara06.png'
    ),
    new Character(
        'Forseti',
        'Imperial Army',
        'X-0, Ausbruch',
        'Special Agent',
        'Chief Strategist',
        'As the Imperial Science Board\'s chief of operations, this cunning and calculating strategist is Ausbruch\'s foil to Walz\'s hot-headedness. Though a disability keeps him from fighting, his pragmatism and twisted sense of justice make him a deadly foe.',
        'http://valkyria.sega.com/img/character/img-chara07.png'
    ),
    new Character(
        'Heinrich Belgar',
        'Imperial Army',
        'X-0, Science Board',
        'Director General',
        'Chief Scientist',
        'As Director General of the Imperial Science Board, this scientific genius has gained favor with the Emperor himself. His personal task force "X-0" is granted free rein to test prototype weapons in battle, much to the dismay of allies and enemies alike.',
        'http://valkyria.sega.com/img/character/img-chara08.png'
    ),
    new Character(
        'Nikola Graf',
        'Imperial Army',
        'X-0, Science Board',
        'Second Lieutenant',
        'Paramilitary Enforcer',
        'Taken in by Dr. Belgar at a young age, this sadistic X-0 enforcer has undergone years of combat training and mental conditioning. Whereas her partner Chiara prefers physical torture, Lt. Graf delights in the subtlety of dealing out psychological torment.',
        'http://valkyria.sega.com/img/character/img-chara09.png'
    )
]