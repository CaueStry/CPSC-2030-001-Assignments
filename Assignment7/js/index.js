window.onload = () => {

    loadCharacters(characters);
    addBatallionEvents(document.getElementById('batallion')
        .getElementsByClassName('character'));
    addSquadEvents(document.getElementById('squad'));

}

function loadCharacters(characters) {
    characters.forEach((character, index) => {
        document.getElementById("batallion").innerHTML += `
            <span class="character ${character.name}">${character.name}</span>
        `;
    });
}

function addBatallionEvents(elements) {
    Array.from(elements).forEach((element) => {
        element.addEventListener("click", () => {
            toggleSquad(element.innerHTML);
        });
        element.addEventListener("mouseenter", () => {
            showInfo(element.innerHTML);
        });
    });
}

function addSquadEvents(element) {
    element.addEventListener('click', (e) => {
        removeFromSquad(e.target.innerHTML);
        document.getElementById('batallion').getElementsByClassName(e.target.innerHTML)[0].classList.remove('selected');
    });
    element.addEventListener('mouseover', (e) => {
        if(element === e.target) return;
        showInfo(e.target.innerHTML);
    });
}

function addToSquad(name) {
    let character = findCharacter(name, characters);
    let squadNum = document.getElementById('squad').getElementsByClassName('character').length;
    if(squadNum >= 5) return;
    if(character) {
        document.getElementById('squad').innerHTML += `<span class="character ${character.name}">${character.name}</span>`;
        document.getElementById('batallion').getElementsByClassName(name)[0].classList.add('selected');
    }
}

function removeFromSquad(name) {
    let element = document.getElementById('squad').getElementsByClassName(name)[0];
    element.parentNode.removeChild(element);
}

function toggleSquad(name) {
    let elements = document.getElementById('squad').getElementsByClassName('character');
    let element = findElement(name, elements);
    if(element) {
        removeFromSquad(name);
        document.getElementById('batallion').getElementsByClassName(name)[0].classList.remove('selected');
    } else {
        addToSquad(name);
    }
}

function showInfo(name) {
    let character = findCharacter(name, characters);
    document.getElementById('profile').innerHTML = `
        <div class="profile-entry">
            <span class="entry-label">Name: </span>
            <span class="entry-data">${character.name}</span>
        </div>
        <div class="profile-entry">
            <span class="entry-label">Side: </span>
            <span class="entry-data">${character.side}</span>
        </div>
        <div class="profile-entry">
            <span class="entry-label">Unit: </span>
            <span class="entry-data">${character.unit}</span>
        </div>
        <div class="profile-entry">
            <span class="entry-label">Rank: </span>
            <span class="entry-data">${character.rank}</span>
        </div>
    `;
    document.getElementById('character-img').innerHTML = `
        <img src="${character.img}">
    `;
}

function findElement(name, elements) {
    return Array.from(elements).find((element) => {
        return element.innerHTML === name;
    });
}

function findCharacter(name, characters) {
    return characters.find(character => character.name === name);
}