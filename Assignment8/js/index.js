$(document).ready(() => {
    $('#menu').click(() => {
        $('#navbar').toggleClass('hide');
    });
    addEvents($('.item'));
});

function addEvents(items) {
    Array.from(items).forEach((item, index) => {
        let toRemove = [];
        for (let i = 0; i <= 5; i++) {
            if(i != index + 1) {
                toRemove.push(`goto${i}`);
            }
        }
        $(item).click((e) => {
            e.preventDefault();
            $('#bullet').removeClass(toRemove).toggleClass(`goto${index + 1}`);
            setTimeout(() => {
                window.location = $(item).attr('href');
            }, 1200);
        });
    });
}