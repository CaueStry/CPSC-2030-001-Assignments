<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Assignment 9</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet/less" type="text/css" href="./styles/styles.less" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.8.1/less.min.js" ></script>
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="./js/index.js"></script>
</head>
<body>
    <main>
        <div class="main-chat">
            <div id="messages"></div>
            <div class="message-input-container">
                <input type="text" id="username" placeholder="Username">
                <input type="text" id="message-input" placeholder="Message...">
                <button id="send-message">Send</button>
            </div>
        </div>
    </main>
</body>
</html>