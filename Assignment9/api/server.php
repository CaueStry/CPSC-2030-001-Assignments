<?php
    $servername = 'localhost';
    $username = 'CPSC2030';
    $password = 'CPSC2030';
    $dbname = 'chat';

    $db = new mysqli($servername, $username, $password, $dbname);

    if ($_SERVER['REQUEST_METHOD'] === 'GET') {
        if(isset($_GET['time'])) {
            $query = 'CALL get_messages('. $_GET['time'] .');';
        } else {
            $query = 'CALL get_messages_last_hour('. (time() * 1000 - 3600) .');';
        }
        $result = $db->query($query);
        if(!$result) {
            die(json_encode(array()));
        }
        $rows = array();
        while($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        header('Content-Type: application/json');
        echo json_encode($rows);

    } else if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if(!isset($_POST['user']) || !isset($_POST['content'])) {
            die("Invalid Request");
        }
        $query = 'CALL send_message("'. $_POST['content'] .'", "'. $_POST['user'] .'", "'. (time() * 1000) .'")';
        $db->query($query);
    }
?>