function getRecentMessages() {
    $.ajax({
        url: "/api/server.php",
        method: "GET",
        dataType: "json"
    })
    .done((messages) => {
        insertMessages(messages);
    })
    .fail(() => {
        throw new Error("Failed to fetch recent messages");
    });
}

function getNewMessages() {
    let lastMessageTime = $(".time-msg").last().attr("time");
    if(!lastMessageTime) {
        lastMessageTime = 0;
    }
    $.ajax({
        url: `/api/server.php?time=${lastMessageTime}`,
        method: "GET",
        dataType: "json"
    })
    .done((messages) => {
        insertMessages(messages);
    })
    .fail(() => {
        throw new Error("Failed to fetch new messages");
    });
}

function sendMessage() {
    if(!$("#username").val() || !$("#message-input").val()) return;
    $.ajax({
        url: "/api/server.php",
        method: "post",
        data: {
            user: $("#username").val(),
            content: $("#message-input").val()
        }
    })
    .done(() => {
        $("#message-input").val("");
    });
    $("#messages").scrollTop($("#messages").height());
}

function insertMessages(messages) {
    let myUsername = $("#username").val();
    messages.reverse().forEach((message) => {
        let formattedTime = `${new Date(parseInt(message.time)).getHours()}:${new Date(parseInt(message.time)).getMinutes()}`;
        if(myUsername == message.user) {
            $("#messages").append(`
                <div class="message" from="me">
                    <span class="username">${message.user}</span>
                    <span class="message-txt">${message.content}</span>
                    <span class="time-msg" time="${message.time}">${formattedTime}</span>
                </div>
            `);
        } else {
            $("#messages").append(`
                <div class="message" from="user">
                    <span class="username">${message.user}</span>
                    <span class="message-txt">${message.content}</span>
                    <span class="time-msg" time="${message.time}">${formattedTime}</span>
                </div>
            `);
        }
        $("#messages").scrollTop($("#messages").height());
    });
}

$(window).on('load', () => {
    getRecentMessages();
    setInterval(getNewMessages, 250);
    $("#send-message").click(sendMessage);
    $("#message-input").keydown((e) => {
        if (e.which === 13) {
            $("#send-message").click();
        }
    });
});