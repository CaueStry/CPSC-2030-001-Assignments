/* Database Creation */
CREATE SCHEMA chat;
USE chat;

/* User Setup */
CREATE USER IF NOT EXISTS 'CPSC2030'@'localhost' IDENTIFIED BY 'CPSC2030';
GRANT ALL PRIVILEGES ON *.* TO 'CPSC2030'@'localhost';
FLUSH PRIVILEGES;

/* Tables */
CREATE TABLE Message (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    content VARCHAR(500),
    time BIGINT,
    user VARCHAR(50)
);

DELIMITER //

CREATE PROCEDURE send_message(IN message_p VARCHAR(500), IN user_p VARCHAR(50), IN time_p BIGINT)
BEGIN
    INSERT INTO Message(content, time, user) VALUES(message_p, time_p, user_p);
END //

CREATE PROCEDURE get_messages_last_hour(IN time_p INTEGER)
BEGIN
    SELECT content, time, user FROM Message
    WHERE time >= time_p
    ORDER BY time DESC
    LIMIT 10;
END //

CREATE PROCEDURE get_messages(IN time_p BIGINT)
BEGIN
    SELECT content, time, user FROM Message
    WHERE time > time_p
    ORDER BY time DESC;
END //

DELIMITER ;