# Babel Fish
https://chatbabelfish.com/

Babel Fish is a web chat application that allows language students to share their knowledge with other students.

## Development

### Stack

* [Node.JS](https://nodejs.org/) - Back-End Logic
* [Express](https://expressjs.com/) - Webserver Framework
* [MariaDB](https://mariadb.org/) - Database
* [Socket.io](https://socket.io/) - Sockets
* [PHP](http://www.php.net/) - View Compiler
* [Twig](https://twig.symfony.com/) - View Model
* [jQuery](https://jquery.com/) - Front-End Logic
* [Axios](https://github.com/axios/axios) - AJAX
* [Less](http://lesscss.org/) - Styling Pre-Processor

### Development Environment Setup

* Make sure you have Node.JS, PHP and MariaDB Installed.
  You should be able to connect to the database with a root account.
```sh
    node -v
    php -v
    mysql -v
```

* Install all dependencies 
```sh
    npm install
```


* Setup the environment variables
```sh
    # HTTP/HTTPS Ports (80 and 443 are default)
    export HTTPPORT=80
    export HTTPSPORT=443
    # Dev Mode (Runs on HTTP)
    export DEV=1
```

* Setup the database
```sh
npm run setupdb
# Type your server's password
```

* Run the server
```sh
node app
```

## Deployment

The application was deployed to AWS EC2, and can be accessed [here](https://chatbabelfish.com/).

## Author

* **Caue Pinheiro** - [CaueStry](https://github.com/CaueStry)

## Purpose

Babel Fish was built as a project for the course CPSC2030 at Langara College.

## License

> This project is licensed under the MIT License - see [LICENSE](LICENSE).