/* Database Creation */
CREATE SCHEMA pokedex;
USE pokedex;

/* User Setup */
CREATE USER IF NOT EXISTS 'CPSC2030'@'localhost' IDENTIFIED BY 'CPSC2030';
GRANT ALL PRIVILEGES ON *.* TO 'CPSC2030'@'localhost';
FLUSH PRIVILEGES;

/* Tables */
CREATE TABLE type (
    type VARCHAR(20),
    PRIMARY KEY (type)
);

CREATE TABLE strength (
    strong VARCHAR(20),
    vulnerable VARCHAR(20),
    PRIMARY KEY (strong, vulnerable),
    FOREIGN KEY (strong) REFERENCES type (type),
    FOREIGN KEY (vulnerable) REFERENCES type (type)
);

CREATE TABLE resistance (
    resistant VARCHAR(20),
    weak VARCHAR(20),
    PRIMARY KEY (resistant, weak),
    FOREIGN KEY (resistant) REFERENCES type (type),
    FOREIGN KEY (weak) REFERENCES type (type)
);

CREATE TABLE pokemon (
    img_url VARCHAR(100),
    pokedex_entry INT,
    name VARCHAR(40) NOT NULL,
    type1 VARCHAR(20) NOT NULL,
    type2 VARCHAR(20),
    hp INT NOT NULL,
    atk INT NOT NULL,
    def INT NOT NULL,
    sat INT NOT NULL,
    sdf INT NOT NULL,
    spd INT NOT NULL,
    bst INT NOT NULL,
    PRIMARY KEY (pokedex_entry),
    FOREIGN KEY (type1) REFERENCES type (type),
    FOREIGN KEY (type2) REFERENCES type (type)
);

CREATE TABLE mega (
    img_url VARCHAR(100),
    pokedex_entry INT,
    name VARCHAR(40) NOT NULL,
    type1 VARCHAR(20) NOT NULL,
    type2 VARCHAR(20),
    hp INT NOT NULL,
    atk INT NOT NULL,
    def INT NOT NULL,
    sat INT NOT NULL,
    sdf INT NOT NULL,
    spd INT NOT NULL,
    bst INT NOT NULL,
    PRIMARY KEY (pokedex_entry),
    FOREIGN KEY (pokedex_entry) REFERENCES pokemon (pokedex_entry),
    FOREIGN KEY (type1) REFERENCES type (type),
    FOREIGN KEY (type2) REFERENCES type (type)
);

CREATE TABLE form (
    img_url VARCHAR(100),
    form_id INT,
    pokedex_entry INT NOT NULL,
    name VARCHAR(40) NOT NULL,
    type1 VARCHAR(20) NOT NULL,
    type2 VARCHAR(20),
    hp INT NOT NULL,
    atk INT NOT NULL,
    def INT NOT NULL,
    sat INT NOT NULL,
    sdf INT NOT NULL,
    spd INT NOT NULL,
    bst INT NOT NULL,
    PRIMARY KEY (pokedex_entry, form_id),
    FOREIGN KEY (pokedex_entry) REFERENCES pokemon (pokedex_entry),
    FOREIGN KEY (type1) REFERENCES type (type),
    FOREIGN KEY (type2) REFERENCES type (type)
);

/* Types Insertion */
INSERT INTO type VALUES('NORMAL');
INSERT INTO type VALUES('FIGHTING');
INSERT INTO type VALUES('FLYING');
INSERT INTO type VALUES('POISON');
INSERT INTO type VALUES('GROUND');
INSERT INTO type VALUES('ROCK');
INSERT INTO type VALUES('BUG');
INSERT INTO type VALUES('GHOST');
INSERT INTO type VALUES('STEEL');
INSERT INTO type VALUES('FIRE');
INSERT INTO type VALUES('WATER');
INSERT INTO type VALUES('GRASS');
INSERT INTO type VALUES('ELECTRIC');
INSERT INTO type VALUES('PSYCHIC');
INSERT INTO type VALUES('ICE');
INSERT INTO type VALUES('DRAGON');
INSERT INTO type VALUES('FAIRY');
INSERT INTO type VALUES('DARK');

/* Strength Isertion */
INSERT INTO strength VALUES('FIGHTING', 'NORMAL');
INSERT INTO strength VALUES('FIGHTING', 'ROCK');
INSERT INTO strength VALUES('FIGHTING', 'STEEL');
INSERT INTO strength VALUES('FIGHTING', 'ICE');
INSERT INTO strength VALUES('FIGHTING', 'DARK');
INSERT INTO strength VALUES('FLYING', 'FIGHTING');
INSERT INTO strength VALUES('FLYING', 'BUG');
INSERT INTO strength VALUES('FLYING', 'GRASS');
INSERT INTO strength VALUES('POISON', 'GRASS');
INSERT INTO strength VALUES('POISON', 'FAIRY');
INSERT INTO strength VALUES('GROUND', 'POISON');
INSERT INTO strength VALUES('GROUND', 'ROCK');
INSERT INTO strength VALUES('GROUND', 'STEEL');
INSERT INTO strength VALUES('GROUND', 'FIRE');
INSERT INTO strength VALUES('GROUND', 'ELECTRIC');
INSERT INTO strength VALUES('ROCK', 'FLYING');
INSERT INTO strength VALUES('ROCK', 'BUG');
INSERT INTO strength VALUES('ROCK', 'FIRE');
INSERT INTO strength VALUES('ROCK', 'ICE');
INSERT INTO strength VALUES('BUG', 'GRASS');
INSERT INTO strength VALUES('BUG', 'PSYCHIC');
INSERT INTO strength VALUES('BUG', 'DARK');
INSERT INTO strength VALUES('GHOST', 'GHOST');
INSERT INTO strength VALUES('GHOST', 'PSYCHIC');
INSERT INTO strength VALUES('STEEL', 'ROCK');
INSERT INTO strength VALUES('STEEL', 'ICE');
INSERT INTO strength VALUES('STEEL', 'FAIRY');
INSERT INTO strength VALUES('FIRE', 'BUG');
INSERT INTO strength VALUES('FIRE', 'STEEL');
INSERT INTO strength VALUES('FIRE', 'GRASS');
INSERT INTO strength VALUES('FIRE', 'ICE');
INSERT INTO strength VALUES('WATER', 'GROUND');
INSERT INTO strength VALUES('WATER', 'ROCK');
INSERT INTO strength VALUES('WATER', 'FIRE');
INSERT INTO strength VALUES('GRASS', 'GROUND');
INSERT INTO strength VALUES('GRASS', 'ROCK');
INSERT INTO strength VALUES('GRASS', 'WATER');
INSERT INTO strength VALUES('ELECTRIC', 'FLYING');
INSERT INTO strength VALUES('ELECTRIC', 'WATER');
INSERT INTO strength VALUES('PSYCHIC', 'FIGHTING');
INSERT INTO strength VALUES('PSYCHIC', 'POISON');
INSERT INTO strength VALUES('ICE', 'FLYING');
INSERT INTO strength VALUES('ICE', 'GROUND');
INSERT INTO strength VALUES('ICE', 'GRASS');
INSERT INTO strength VALUES('ICE', 'DRAGON');
INSERT INTO strength VALUES('DRAGON', 'DRAGON');
INSERT INTO strength VALUES('FAIRY', 'FIGHTING');
INSERT INTO strength VALUES('FAIRY', 'DRAGON');
INSERT INTO strength VALUES('FAIRY', 'DARK');
INSERT INTO strength VALUES('DARK', 'GHOST');
INSERT INTO strength VALUES('DARK', 'PSYCHIC');

/* Resistance Inserion */
INSERT INTO resistance VALUES('NORMAL', 'GHOST');
INSERT INTO resistance VALUES('FIGHTING', 'ROCK');
INSERT INTO resistance VALUES('FIGHTING', 'BUG');
INSERT INTO resistance VALUES('FIGHTING', 'DARK');
INSERT INTO resistance VALUES('FLYING', 'FIGHTING');
INSERT INTO resistance VALUES('FLYING', 'GROUND');
INSERT INTO resistance VALUES('FLYING', 'BUG');
INSERT INTO resistance VALUES('FLYING', 'GRASS');
INSERT INTO resistance VALUES('POISON', 'FIGHTING');
INSERT INTO resistance VALUES('POISON', 'POISON');
INSERT INTO resistance VALUES('POISON', 'GRASS');
INSERT INTO resistance VALUES('POISON', 'FAIRY');
INSERT INTO resistance VALUES('GROUND', 'POISON');
INSERT INTO resistance VALUES('GROUND', 'ROCK');
INSERT INTO resistance VALUES('GROUND', 'ELECTRIC');
INSERT INTO resistance VALUES('ROCK', 'NORMAL');
INSERT INTO resistance VALUES('ROCK', 'FLYING');
INSERT INTO resistance VALUES('ROCK', 'POISON');
INSERT INTO resistance VALUES('ROCK', 'FIRE');
INSERT INTO resistance VALUES('BUG', 'FIGHTING');
INSERT INTO resistance VALUES('BUG', 'GROUND');
INSERT INTO resistance VALUES('BUG', 'GRASS');
INSERT INTO resistance VALUES('GHOST', 'NORMAL');
INSERT INTO resistance VALUES('GHOST', 'FIGHTING');
INSERT INTO resistance VALUES('GHOST', 'POISON');
INSERT INTO resistance VALUES('GHOST', 'BUG');
INSERT INTO resistance VALUES('STEEL', 'NORMAL');
INSERT INTO resistance VALUES('STEEL', 'FLYING');
INSERT INTO resistance VALUES('STEEL', 'POISON');
INSERT INTO resistance VALUES('STEEL', 'ROCK');
INSERT INTO resistance VALUES('STEEL', 'BUG');
INSERT INTO resistance VALUES('STEEL', 'STEEL');
INSERT INTO resistance VALUES('STEEL', 'GRASS');
INSERT INTO resistance VALUES('STEEL', 'PSYCHIC');
INSERT INTO resistance VALUES('STEEL', 'ICE');
INSERT INTO resistance VALUES('STEEL', 'DRAGON');
INSERT INTO resistance VALUES('STEEL', 'FAIRY');
INSERT INTO resistance VALUES('FIRE', 'BUG');
INSERT INTO resistance VALUES('FIRE', 'STEEL');
INSERT INTO resistance VALUES('FIRE', 'FIRE');
INSERT INTO resistance VALUES('FIRE', 'GRASS');
INSERT INTO resistance VALUES('FIRE', 'ICE');
INSERT INTO resistance VALUES('WATER', 'STEEL');
INSERT INTO resistance VALUES('WATER', 'FIRE');
INSERT INTO resistance VALUES('WATER', 'WATER');
INSERT INTO resistance VALUES('WATER', 'ICE');
INSERT INTO resistance VALUES('GRASS', 'GROUND');
INSERT INTO resistance VALUES('GRASS', 'WATER');
INSERT INTO resistance VALUES('GRASS', 'GRASS');
INSERT INTO resistance VALUES('GRASS', 'ELECTRIC');
INSERT INTO resistance VALUES('ELECTRIC', 'FLYING');
INSERT INTO resistance VALUES('ELECTRIC', 'STEEL');
INSERT INTO resistance VALUES('ELECTRIC', 'ELECTRIC');
INSERT INTO resistance VALUES('PSYCHIC', 'FIGHTING');
INSERT INTO resistance VALUES('PSYCHIC', 'PSYCHIC');
INSERT INTO resistance VALUES('ICE', 'ICE');
INSERT INTO resistance VALUES('DRAGON', 'FIRE');
INSERT INTO resistance VALUES('DRAGON', 'WATER');
INSERT INTO resistance VALUES('DRAGON', 'GRASS');
INSERT INTO resistance VALUES('DRAGON', 'ELECTRIC');
INSERT INTO resistance VALUES('FAIRY', 'FIGHTING');
INSERT INTO resistance VALUES('FAIRY', 'BUG');
INSERT INTO resistance VALUES('FAIRY', 'DRAGON');
INSERT INTO resistance VALUES('FAIRY', 'DARK');
INSERT INTO resistance VALUES('DARK', 'GHOST');
INSERT INTO resistance VALUES('DARK', 'PSYCHIC');
INSERT INTO resistance VALUES('DARK', 'DARK');

/* Pokemons Insertion */
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/194/215/199/387.png', 387, 'Turtwig', 'GRASS', NULL, 55, 68, 64, 45, 55, 31, 318);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/195/215/199/388.png', 388, 'Grotle', 'GRASS', NULL, 75, 89, 85, 55, 65, 36, 405);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/196/215/199/389.png', 389, 'Torterra', 'GRASS', 'GROUND', 95, 109, 105, 75, 85, 56, 525);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/197/215/199/390.png', 390, 'Chimchar', 'FIRE', NULL, 44, 58, 44, 58, 44, 61, 309);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/198/215/199/391.png', 391, 'Monferno', 'FIRE', 'FIGHTING', 64, 78, 52, 78, 52, 81, 405);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/199/215/199/392.png', 392, 'Infernape', 'FIRE', 'FIGHTING', 76, 104, 71, 104, 71, 108, 534);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/200/215/199/393.png', 393, 'Piplup', 'WATER', NULL, 53, 51, 53, 61, 56, 40, 314);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/201/215/199/394.png', 394, 'Prinplup', 'WATER', NULL, 64, 66, 68, 81, 76, 50, 405);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/202/215/199/395.png', 395, 'Empoleon', 'WATER', 'STEEL', 84, 86, 88, 111, 101, 60, 530);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/203/215/199/396.png', 396, 'Starly', 'NORMAL', 'FLYING', 40, 55, 30, 30, 30, 60, 245);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/204/215/199/397.png', 397, 'Staravia', 'NORMAL', 'FLYING', 55, 75, 50, 40, 40, 80, 340);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/205/215/199/398.png', 398, 'Staraptor', 'NORMAL', 'FLYING', 85, 120, 70, 50, 60, 100, 485);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/206/215/199/399.png', 399, 'Bidoof', 'NORMAL', NULL, 59, 45, 40, 35, 40, 31, 250);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/207/215/199/400.png', 400, 'Bibarel', 'NORMAL', 'WATER', 79, 85, 60, 55, 60, 71, 410);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/208/215/199/401.png', 401, 'Kricketot', 'BUG', NULL, 37, 25, 41, 25, 41, 25, 194);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/209/215/199/402.png', 402, 'Kricketune', 'BUG', NULL, 77, 85, 51, 55, 51, 65, 384);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/210/215/199/403.png', 403, 'Shinx', 'ELECTRIC', NULL, 45, 65, 34, 40, 34, 45, 263);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/211/215/199/404.png', 404, 'Luxio', 'ELECTRIC', NULL, 60, 85, 49, 60, 49, 60, 363);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/212/215/199/405.png', 405, 'Luxray', 'ELECTRIC', NULL, 80, 120, 79, 95, 79, 70, 523);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/213/215/199/406.png', 406, 'Budew', 'GRASS', 'POISON', 40, 30, 35, 50, 70, 55, 280);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/214/215/199/407.png', 407, 'Roserade', 'GRASS', 'POISON', 60, 70, 65, 125, 105, 90, 515);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/215/215/199/408.png', 408, 'Cranidos', 'ROCK', NULL, 67, 125, 40, 30, 30, 58, 350);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/216/215/199/409.png', 409, 'Rampardos', 'ROCK', NULL, 97, 165, 60, 65, 50, 58, 495);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/217/215/199/410.png', 410, 'Shieldon', 'ROCK', 'STEEL', 30, 42, 118, 42, 88, 30, 350);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/218/215/199/411.png', 411, 'Bastiodon', 'ROCK', 'STEEL', 60, 52, 168, 47, 138, 30, 495);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/219/215/199/412.png', 412, 'Burmy', 'BUG', NULL, 40, 29, 45, 29, 45, 36, 224);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/220/215/199/413.png', 413, 'Wormadam', 'BUG', 'GRASS', 60, 59, 85, 79, 105, 36, 424);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/221/215/199/414.png', 414, 'Mothim', 'BUG', 'FLYING', 70, 94, 50, 94, 50, 66, 424);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/222/215/199/415.png', 415, 'Combee', 'BUG', 'FLYING', 30, 30, 42, 30, 42, 70, 244);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/223/215/199/416.png', 416, 'Vespiquen', 'BUG', 'FLYING', 70, 80, 102, 80, 102, 40, 474);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/224/215/199/417.png', 417, 'Pachirisu', 'ELECTRIC', NULL, 60, 45, 70, 45, 90, 95, 405);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/225/215/199/418.png', 418, 'Buizel', 'WATER', NULL, 55, 65, 35, 60, 30, 85, 330);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/226/215/199/419.png', 419, 'Floatzel', 'WATER', NULL, 85, 105, 55, 85, 50, 115, 495);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/227/215/199/420.png', 420, 'Cherubi', 'GRASS', NULL, 45, 35, 45, 62, 53, 35, 275);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/228/215/199/421.png', 421, 'Cherrim', 'GRASS', NULL, 70, 60, 70, 87, 78, 85, 450);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/229/215/199/422.png', 422, 'Shellos', 'WATER', NULL, 76, 48, 48, 57, 62, 34, 325);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/230/215/199/423.png', 423, 'Gastrodon', 'WATER', 'GROUND', 111, 83, 68, 92, 82, 39, 475);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/231/215/199/424.png', 424, 'Ambipom', 'NORMAL', NULL, 75, 100, 66, 60, 66, 115, 482);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/232/215/199/425.png', 425, 'Drifloon', 'GHOST', 'FLYING', 90, 50, 34, 60, 44, 70, 348);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/233/215/199/426.png', 426, 'Drifblim', 'GHOST', 'FLYING', 150, 80, 44, 90, 54, 80, 498);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/234/215/199/427.png', 427, 'Buneary', 'NORMAL', NULL, 55, 66, 44, 44, 56, 85, 350);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/235/215/199/428.png', 428, 'Lopunny', 'NORMAL', NULL, 65, 76, 84, 54, 96, 105, 480);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/236/215/199/429.png', 429, 'Mismagius', 'GHOST', NULL, 60, 60, 60, 105, 105, 105, 495);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/237/215/199/430.png', 430, 'Honchkrow', 'DARK', 'FLYING', 100, 125, 52, 105, 52, 71, 505);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/238/215/199/431.png', 431, 'Glameow', 'NORMAL', NULL, 49, 55, 42, 42, 37, 85, 310);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/239/215/199/432.png', 432, 'Purugly', 'NORMAL', NULL, 71, 82, 64, 64, 59, 112, 452);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/240/215/199/433.png', 433, 'Chingling', 'PSYCHIC', NULL, 45, 30, 50, 65, 50, 45, 285);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/241/215/199/434.png', 434, 'Stunky', 'POISON', 'DARK', 63, 63, 47, 41, 41, 74, 329);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/242/215/199/435.png', 435, 'Skuntank', 'POISON', 'DARK', 103, 93, 67, 71, 61, 84, 479);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/243/215/199/436.png', 436, 'Bronzor', 'STEEL', 'PSYCHIC', 57, 24, 86, 24, 86, 23, 300);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/244/215/199/437.png', 437, 'Bronzong', 'STEEL', 'PSYCHIC', 67, 89, 116, 79, 116, 33, 500);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/245/215/199/438.png', 438, 'Bonsly', 'ROCK', NULL, 50, 80, 95, 10, 45, 10, 290);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/246/215/199/439.png', 439, 'Mime Jr.', 'PSYCHIC', 'FAIRY', 20, 25, 45, 70, 90, 60, 310);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/247/215/199/440.png', 440, 'Happiny', 'NORMAL', NULL, 100, 5, 5, 15, 65, 30, 220);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/248/215/199/441.png', 441, 'Chatot', 'NORMAL', 'FLYING', 76, 65, 45, 92, 42, 91, 411);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/249/215/199/442.png', 442, 'Spiritomb', 'GHOST', 'DARK', 50, 92, 108, 92, 108, 35, 485);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/250/215/199/443.png', 443, 'Gible', 'DRAGON', 'GROUND', 58, 70, 45, 40, 45, 42, 300);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/251/215/199/444.png', 444, 'Gabite', 'DRAGON', 'GROUND', 68, 90, 65, 50, 55, 82, 410);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/252/215/199/445.png', 445, 'Garchomp', 'DRAGON', 'GROUND', 108, 130, 95, 80, 85, 102, 600);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/253/215/199/446.png', 446, 'Munchlax', 'NORMAL', NULL, 135, 85, 40, 40, 85, 5, 390);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/254/215/199/447.png', 447, 'Riolu', 'FIGHTING', NULL, 40, 70, 40, 35, 40, 60, 285);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/255/215/199/448.png', 448, 'Lucario', 'FIGHTING', 'STEEL', 70, 110, 70, 115, 70, 90, 525);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/256/215/199/449.png', 449, 'Hippopotas', 'GROUND', NULL, 68, 72, 78, 38, 42, 32, 330);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/257/215/199/450.png', 450, 'Hippowdon', 'GROUND', NULL, 108, 112, 118, 68, 72, 47, 525);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/258/215/199/451.png', 451, 'Skorupi', 'POISON', 'BUG', 40, 50, 90, 30, 55, 65, 330);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/259/215/199/452.png', 452, 'Drapion', 'POISON', 'DARK', 70, 90, 110, 60, 75, 95, 500);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/260/215/199/453.png', 453, 'Croagunk', 'POISON', 'FIGHTING', 48, 61, 40, 61, 40, 50, 300);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/261/215/199/454.png', 454, 'Toxicroak', 'POISON', 'FIGHTING', 83, 106, 65, 86, 65, 85, 490);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/262/215/199/455.png', 455, 'Carnivine', 'GRASS', NULL, 74, 100, 72, 90, 72, 46, 454);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/263/215/199/456.png', 456, 'Finneon', 'WATER', NULL, 49, 49, 56, 49, 61, 66, 330);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/264/215/199/457.png', 457, 'Lumineon', 'WATER', NULL, 69, 69, 76, 69, 86, 91, 460);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/265/215/199/458.png', 458, 'Mantyke', 'WATER', 'FLYING', 45, 20, 50, 60, 120, 50, 345);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/266/215/199/459.png', 459, 'Snover', 'GRASS', 'ICE', 60, 62, 50, 62, 60, 40, 334);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/267/215/199/460.png', 460, 'Abomasnow', 'GRASS', 'ICE', 90, 92, 75, 92, 85, 60, 494);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/268/215/199/461.png', 461, 'Weavile', 'DARK', 'ICE', 70, 120, 65, 45, 85, 125, 510);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/269/215/199/462.png', 462, 'Magnezone', 'ELECTRIC', 'STEEL', 70, 70, 115, 130, 90, 60, 535);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/270/215/199/463.png', 463, 'Lickilicky', 'NORMAL', NULL, 110, 85, 95, 80, 95, 50, 515);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/271/215/199/464.png', 464, 'Rhyperior', 'GROUND', 'ROCK', 115, 140, 130, 55, 55, 40, 535);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/272/215/199/465.png', 465, 'Tangrowth', 'GRASS', NULL, 100, 100, 125, 110, 50, 50, 535);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/273/215/199/466.png', 466, 'Electivire', 'ELECTRIC', NULL, 75, 123, 67, 95, 85, 95, 540);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/274/215/199/467.png', 467, 'Magmortar', 'FIRE', NULL, 75, 95, 67, 125, 95, 83, 540);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/275/215/199/468.png', 468, 'Togekiss', 'FAIRY', 'FLYING', 85, 50, 95, 120, 115, 80, 545);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/276/215/199/469.png', 469, 'Yanmega', 'BUG', 'FLYING', 86, 76, 86, 116, 56, 95, 515);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/277/215/199/470.png', 470, 'Leafeon', 'GRASS', NULL, 65, 110, 130, 60, 65, 95, 525);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/278/215/199/471.png', 471, 'Glaceon', 'ICE', NULL, 65, 60, 110, 130, 95, 65, 525);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/279/215/199/472.png', 472, 'Gliscor', 'GROUND', 'FLYING', 75, 95, 125, 45, 75, 95, 510);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/280/215/199/473.png', 473, 'Mamoswine', 'ICE', 'GROUND', 110, 130, 80, 70, 60, 80, 530);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/281/215/199/474.png', 474, 'Porygon-Z', 'NORMAL', NULL, 85, 80, 70, 135, 75, 90, 535);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/282/215/199/475.png', 475, 'Gallade', 'PSYCHIC', 'FIGHTING', 68, 125, 65, 65, 115, 80, 518);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/283/215/199/476.png', 476, 'Probopass', 'ROCK', 'STEEL', 60, 55, 145, 75, 150, 40, 525);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/284/215/199/477.png', 477, 'Dusknoir', 'GHOST', NULL, 45, 100, 135, 65, 135, 45, 525);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/285/215/199/478.png', 478, 'Froslass', 'ICE', 'GHOST', 70, 80, 70, 80, 70, 110, 480);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', 479, 'Rotom', 'ELECTRIC', 'GHOST', 50, 50, 77, 95, 77, 91, 440);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/287/215/199/480.png', 480, 'Uxie', 'PSYCHIC', NULL, 75, 75, 130, 75, 130, 95, 580);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/288/215/199/481.png', 481, 'Mesprit', 'PSYCHIC', NULL, 80, 105, 105, 105, 105, 80, 580);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/289/215/199/482.png', 482, 'Azelf', 'PSYCHIC', NULL, 75, 125, 70, 125, 70, 115, 580);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/290/215/199/483.png', 483, 'Dialga', 'STEEL', 'DRAGON', 100, 120, 120, 150, 100, 90, 680);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/291/215/199/484.png', 484, 'Palkia', 'WATER', 'DRAGON', 90, 120, 100, 150, 120, 100, 680);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/292/215/199/485.png', 485, 'Heatran', 'FIRE', 'STEEL', 91, 90, 106, 130, 106, 77, 600);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/293/215/199/486.png', 486, 'Regigigas', 'NORMAL', NULL, 110, 160, 110, 80, 110, 100, 670);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/294/215/199/487.png', 487, 'Giratina', 'GHOST', 'DRAGON', 150, 100, 120, 100, 120, 90, 680);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/295/215/199/488.png', 488, 'Cresselia', 'PSYCHIC', NULL, 120, 70, 120, 75, 130, 85, 600);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/296/215/199/489.png', 489, 'Phione', 'WATER', NULL, 80, 80, 80, 80, 80, 80, 480);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/297/215/199/490.png', 490, 'Manaphy', 'WATER', NULL, 100, 100, 100, 100, 100, 100, 600);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/298/215/199/491.png', 491, 'Darkrai', 'DARK', NULL, 70, 90, 90, 135, 90, 125, 600);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/299/215/199/492.png', 492, 'Shaymin', 'GRASS', NULL, 100, 100, 100, 100, 100, 100, 600);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/300/215/199/493.png', 493, 'Arceus', 'NORMAL', NULL, 120, 120, 120, 120, 120, 120, 720);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/301/215/199/494.png', 494, 'Victini', 'PSYCHIC', 'FIRE', 100, 100, 100, 100, 100, 100, 600);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/302/215/199/495.png', 495, 'Snivy', 'GRASS', NULL, 45, 45, 55, 45, 55, 63, 308);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/303/215/199/496.png', 496, 'Servine', 'GRASS', NULL, 60, 60, 75, 60, 75, 83, 413);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/304/215/199/497.png', 497, 'Serperior', 'GRASS', NULL, 75, 75, 95, 75, 95, 113, 528);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/305/215/199/498.png', 498, 'Tepig', 'FIRE', NULL, 65, 63, 45, 45, 45, 45, 308);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/306/215/199/499.png', 499, 'Pignite', 'FIRE', 'FIGHTING', 90, 93, 55, 70, 55, 55, 418);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/307/215/199/500.png', 500, 'Emboar', 'FIRE', 'FIGHTING', 110, 123, 65, 100, 65, 65, 528);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/308/215/199/501.png', 501, 'Oshawott', 'WATER', NULL, 55, 55, 45, 63, 45, 45, 308);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/309/215/199/502.png', 502, 'Dewott', 'WATER', NULL, 75, 75, 60, 83, 60, 60, 413);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/310/215/199/503.png', 503, 'Samurott', 'WATER', NULL, 95, 100, 85, 108, 70, 70, 528);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/311/215/199/504.png', 504, 'Patrat', 'NORMAL', NULL, 45, 55, 39, 35, 39, 42, 255);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/312/215/199/505.png', 505, 'Watchog', 'NORMAL', NULL, 60, 85, 69, 60, 69, 77, 420);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/313/215/199/506.png', 506, 'Lillipup', 'NORMAL', NULL, 45, 60, 45, 25, 45, 55, 275);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/314/215/199/507.png', 507, 'Herdier', 'NORMAL', NULL, 65, 80, 65, 35, 65, 60, 370);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/315/215/199/508.png', 508, 'Stoutland', 'NORMAL', NULL, 85, 110, 90, 45, 90, 80, 500);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/316/215/199/509.png', 509, 'Purrloin', 'DARK', NULL, 41, 50, 37, 50, 37, 66, 281);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/317/215/199/510.png', 510, 'Liepard', 'DARK', NULL, 64, 88, 50, 88, 50, 106, 446);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/318/215/199/511.png', 511, 'Pansage', 'GRASS', NULL, 50, 53, 48, 53, 48, 64, 316);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/319/215/199/512.png', 512, 'Simisage', 'GRASS', NULL, 75, 98, 63, 98, 63, 101, 498);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/320/215/199/513.png', 513, 'Pansear', 'FIRE', NULL, 50, 53, 48, 53, 48, 64, 316);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/321/215/199/514.png', 514, 'Simisear', 'FIRE', NULL, 75, 98, 63, 98, 63, 101, 498);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/322/215/199/515.png', 515, 'Panpour', 'WATER', NULL, 50, 53, 48, 53, 48, 64, 316);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/323/215/199/516.png', 516, 'Simipour', 'WATER', NULL, 75, 98, 63, 98, 63, 101, 498);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/324/215/199/517.png', 517, 'Munna', 'PSYCHIC', NULL, 76, 25, 45, 67, 55, 24, 292);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/325/215/199/518.png', 518, 'Musharna', 'PSYCHIC', NULL, 116, 55, 85, 107, 95, 29, 487);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/326/215/199/519.png', 519, 'Pidove', 'NORMAL', 'FLYING', 50, 55, 50, 36, 30, 43, 264);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/327/215/199/520.png', 520, 'Tranquill', 'NORMAL', 'FLYING', 62, 77, 62, 50, 42, 65, 358);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/328/215/199/521.png', 521, 'Unfezant', 'NORMAL', 'FLYING', 80, 115, 80, 65, 55, 93, 488);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/329/215/199/522.png', 522, 'Blitzle', 'ELECTRIC', NULL, 45, 60, 32, 50, 32, 76, 295);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/330/215/199/523.png', 523, 'Zebstrika', 'ELECTRIC', NULL, 75, 100, 63, 80, 63, 116, 497);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/331/215/199/524.png', 524, 'Roggenrola', 'ROCK', NULL, 55, 75, 85, 25, 25, 15, 280);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/332/215/199/525.png', 525, 'Boldore', 'ROCK', NULL, 70, 105, 105, 50, 40, 20, 390);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/333/215/199/526.png', 526, 'Gigalith', 'ROCK', NULL, 85, 135, 130, 60, 80, 25, 515);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/334/215/199/527.png', 527, 'Woobat', 'PSYCHIC', 'FLYING', 55, 45, 43, 55, 43, 72, 313);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/335/215/199/528.png', 528, 'Swoobat', 'PSYCHIC', 'FLYING', 67, 57, 55, 77, 55, 114, 425);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/336/215/199/529.png', 529, 'Drilbur', 'GROUND', NULL, 60, 85, 40, 30, 45, 68, 328);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/337/215/199/530.png', 530, 'Excadrill', 'GROUND', 'STEEL', 110, 135, 60, 50, 65, 88, 508);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/338/215/199/531.png', 531, 'Audino', 'NORMAL', NULL, 103, 60, 86, 60, 86, 50, 445);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/339/215/199/532.png', 532, 'Timburr', 'FIGHTING', NULL, 75, 80, 55, 25, 35, 35, 305);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/340/215/199/533.png', 533, 'Gurdurr', 'FIGHTING', NULL, 85, 105, 85, 40, 50, 40, 405);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/341/215/199/534.png', 534, 'Conkeldurr', 'FIGHTING', NULL, 105, 140, 95, 55, 65, 45, 505);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/342/215/199/535.png', 535, 'Tympole', 'WATER', NULL, 50, 50, 40, 50, 40, 64, 294);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/343/215/199/536.png', 536, 'Palpitoad', 'WATER', 'GROUND', 75, 65, 55, 65, 55, 69, 384);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/344/215/199/537.png', 537, 'Seismitoad', 'WATER', 'GROUND', 105, 95, 75, 85, 75, 74, 509);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/345/215/199/538.png', 538, 'Throh', 'FIGHTING', NULL, 120, 100, 85, 30, 85, 45, 465);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/346/215/199/539.png', 539, 'Sawk', 'FIGHTING', NULL, 75, 125, 75, 30, 75, 85, 465);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/347/215/199/540.png', 540, 'Sewaddle', 'BUG', 'GRASS', 45, 53, 70, 40, 60, 42, 310);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/348/215/199/541.png', 541, 'Swadloon', 'BUG', 'GRASS', 55, 63, 90, 50, 80, 42, 380);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/349/215/199/542.png', 542, 'Leavanny', 'BUG', 'GRASS', 75, 103, 80, 70, 80, 92, 500);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/350/215/199/543.png', 543, 'Venipede', 'BUG', 'POISON', 30, 45, 59, 30, 39, 57, 260);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/351/215/199/544.png', 544, 'Whirlipede', 'BUG', 'POISON', 40, 55, 99, 40, 79, 47, 360);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/352/215/199/545.png', 545, 'Scolipede', 'BUG', 'POISON', 60, 100, 89, 55, 69, 112, 485);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/353/215/199/546.png', 546, 'Cottonee', 'GRASS', 'FAIRY', 40, 27, 60, 37, 50, 66, 280);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/354/215/199/547.png', 547, 'Whimsicott', 'GRASS', 'FAIRY', 60, 67, 85, 77, 75, 116, 480);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/355/215/199/548.png', 548, 'Petilil', 'GRASS', NULL, 45, 35, 50, 70, 50, 30, 280);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/356/215/199/549.png', 549, 'Lilligant', 'GRASS', NULL, 70, 60, 75, 110, 75, 90, 480);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/357/215/199/550.png', 550, 'Basculin', 'WATER', NULL, 70, 92, 65, 80, 55, 98, 460);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/358/215/199/551.png', 551, 'Sandile', 'GROUND', 'DARK', 50, 72, 35, 35, 35, 65, 292);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/359/215/199/552.png', 552, 'Krokorok', 'GROUND', 'DARK', 60, 82, 45, 45, 45, 74, 351);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/360/215/199/553.png', 553, 'Krookodile', 'GROUND', 'DARK', 95, 117, 80, 65, 70, 92, 519);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/361/215/199/554.png', 554, 'Darumaka', 'FIRE', NULL, 70, 90, 45, 15, 45, 50, 315);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/362/215/199/555.png', 555, 'Darmanitan', 'FIRE', NULL, 105, 140, 55, 30, 55, 95, 480);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/363/215/199/556.png', 556, 'Maractus', 'GRASS', NULL, 75, 86, 67, 106, 67, 60, 461);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/364/215/199/557.png', 557, 'Dwebble', 'BUG', 'ROCK', 50, 65, 85, 35, 35, 55, 325);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/365/215/199/558.png', 558, 'Crustle', 'BUG', 'ROCK', 70, 95, 125, 65, 75, 45, 475);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/366/215/199/559.png', 559, 'Scraggy', 'DARK', 'FIGHTING', 50, 75, 70, 35, 70, 48, 348);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/367/215/199/560.png', 560, 'Scrafty', 'DARK', 'FIGHTING', 65, 90, 115, 45, 115, 58, 488);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/368/215/199/561.png', 561, 'Sigilyph', 'PSYCHIC', 'FLYING', 72, 58, 80, 103, 80, 97, 490);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/369/215/199/562.png', 562, 'Yamask', 'GHOST', NULL, 38, 30, 85, 55, 65, 30, 303);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/370/215/199/563.png', 563, 'Cofagrigus', 'GHOST', NULL, 58, 50, 145, 95, 105, 30, 483);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/371/215/199/564.png', 564, 'Tirtouga', 'WATER', 'ROCK', 54, 78, 103, 53, 45, 22, 355);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/372/215/199/565.png', 565, 'Carracosta', 'WATER', 'ROCK', 74, 108, 133, 83, 65, 32, 495);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/373/215/199/566.png', 566, 'Archen', 'ROCK', 'FLYING', 55, 112, 45, 74, 45, 70, 401);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/374/215/199/567.png', 567, 'Archeops', 'ROCK', 'FLYING', 75, 140, 65, 112, 65, 110, 567);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/375/215/199/568.png', 568, 'Trubbish', 'POISON', NULL, 50, 50, 62, 40, 62, 65, 329);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/376/215/199/569.png', 569, 'Garbodor', 'POISON', NULL, 80, 95, 82, 60, 82, 75, 474);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/377/215/199/570.png', 570, 'Zorua', 'DARK', NULL, 40, 65, 40, 80, 40, 65, 330);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/378/215/199/571.png', 571, 'Zoroark', 'DARK', NULL, 60, 105, 60, 120, 60, 105, 510);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/379/215/199/572.png', 572, 'Minccino', 'NORMAL', NULL, 55, 50, 40, 40, 40, 75, 300);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/380/215/199/573.png', 573, 'Cinccino', 'NORMAL', NULL, 75, 95, 60, 65, 60, 115, 470);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/381/215/199/574.png', 574, 'Gothita', 'PSYCHIC', NULL, 45, 30, 50, 55, 65, 45, 290);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/382/215/199/575.png', 575, 'Gothorita', 'PSYCHIC', NULL, 60, 45, 70, 75, 85, 55, 390);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/383/215/199/576.png', 576, 'Gothitelle', 'PSYCHIC', NULL, 70, 55, 95, 95, 110, 65, 490);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/384/215/199/577.png', 577, 'Solosis', 'PSYCHIC', NULL, 45, 30, 40, 105, 50, 20, 290);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/385/215/199/578.png', 578, 'Duosion', 'PSYCHIC', NULL, 65, 40, 50, 125, 60, 30, 370);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/386/215/199/579.png', 579, 'Reuniclus', 'PSYCHIC', NULL, 110, 65, 75, 125, 85, 30, 490);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/387/215/199/580.png', 580, 'Ducklett', 'WATER', 'FLYING', 62, 44, 50, 44, 50, 55, 305);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/388/215/199/581.png', 581, 'Swanna', 'WATER', 'FLYING', 75, 87, 63, 87, 63, 98, 473);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/389/215/199/582.png', 582, 'Vanillite', 'ICE', NULL, 36, 50, 50, 65, 60, 44, 305);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/390/215/199/583.png', 583, 'Vanillish', 'ICE', NULL, 51, 65, 65, 80, 75, 59, 395);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/391/215/199/584.png', 584, 'Vanilluxe', 'ICE', NULL, 71, 95, 85, 110, 95, 79, 535);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/392/215/199/585.png', 585, 'Deerling', 'NORMAL', 'GRASS', 60, 60, 50, 40, 50, 75, 335);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/393/215/199/586.png', 586, 'Sawsbuck', 'NORMAL', 'GRASS', 80, 100, 70, 60, 70, 95, 475);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/394/215/199/587.png', 587, 'Emolga', 'ELECTRIC', 'FLYING', 55, 75, 60, 75, 60, 103, 428);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/395/215/199/588.png', 588, 'Karrablast', 'BUG', NULL, 50, 75, 45, 40, 45, 60, 315);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/396/215/199/589.png', 589, 'Escavalier', 'BUG', 'STEEL', 70, 135, 105, 60, 105, 20, 495);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/397/215/199/590.png', 590, 'Foongus', 'GRASS', 'POISON', 69, 55, 45, 55, 55, 15, 294);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/398/215/199/591.png', 591, 'Amoonguss', 'GRASS', 'POISON', 114, 85, 70, 85, 80, 30, 464);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/399/215/199/592.png', 592, 'Frillish', 'WATER', 'GHOST', 55, 40, 50, 65, 85, 40, 335);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/400/215/199/593.png', 593, 'Jellicent', 'WATER', 'GHOST', 100, 60, 70, 85, 105, 60, 480);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/401/215/199/594.png', 594, 'Alomomola', 'WATER', NULL, 165, 75, 80, 40, 45, 65, 470);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/402/215/199/595.png', 595, 'Joltik', 'BUG', 'ELECTRIC', 50, 47, 50, 57, 50, 65, 319);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/403/215/199/596.png', 596, 'Galvantula', 'BUG', 'ELECTRIC', 70, 77, 60, 97, 60, 108, 472);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/404/215/199/597.png', 597, 'Ferroseed', 'GRASS', 'STEEL', 44, 50, 91, 24, 86, 10, 305);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/405/215/199/598.png', 598, 'Ferrothorn', 'GRASS', 'STEEL', 74, 94, 131, 54, 116, 20, 489);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/406/215/199/599.png', 599, 'Klink', 'STEEL', NULL, 40, 55, 70, 45, 60, 30, 300);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/407/215/199/600.png', 600, 'Klang', 'STEEL', NULL, 60, 80, 95, 70, 85, 50, 440);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/408/215/199/601.png', 601, 'Klinklang', 'STEEL', NULL, 60, 100, 115, 70, 85, 90, 520);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/409/215/199/602.png', 602, 'Tynamo', 'ELECTRIC', NULL, 35, 55, 40, 45, 40, 60, 275);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/410/215/199/603.png', 603, 'Eelektrik', 'ELECTRIC', NULL, 65, 85, 70, 75, 70, 40, 405);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/411/215/199/604.png', 604, 'Eelektross', 'ELECTRIC', NULL, 85, 115, 80, 105, 80, 50, 515);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/412/215/199/605.png', 605, 'Elgyem', 'PSYCHIC', NULL, 55, 55, 55, 85, 55, 30, 335);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/413/215/199/606.png', 606, 'Beheeyem', 'PSYCHIC', NULL, 75, 75, 75, 125, 95, 40, 485);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/414/215/199/607.png', 607, 'Litwick', 'GHOST', 'FIRE', 50, 30, 55, 65, 55, 20, 275);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/415/215/199/608.png', 608, 'Lampent', 'GHOST', 'FIRE', 60, 40, 60, 95, 60, 55, 370);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/416/215/199/609.png', 609, 'Chandelure', 'GHOST', 'FIRE', 60, 55, 90, 145, 90, 80, 520);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/417/215/199/610.png', 610, 'Axew', 'DRAGON', NULL, 46, 87, 60, 30, 40, 57, 320);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/418/215/199/611.png', 611, 'Fraxure', 'DRAGON', NULL, 66, 117, 70, 40, 50, 67, 410);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/419/215/199/612.png', 612, 'Haxorus', 'DRAGON', NULL, 76, 147, 90, 60, 70, 97, 540);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/420/215/199/613.png', 613, 'Cubchoo', 'ICE', NULL, 55, 70, 40, 60, 40, 40, 305);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/421/215/199/614.png', 614, 'Beartic', 'ICE', NULL, 95, 110, 80, 70, 80, 50, 485);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/422/215/199/615.png', 615, 'Cryogonal', 'ICE', NULL, 70, 50, 30, 95, 135, 105, 485);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/423/215/199/616.png', 616, 'Shelmet', 'BUG', NULL, 50, 40, 85, 40, 65, 25, 305);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/424/215/199/617.png', 617, 'Accelgor', 'BUG', NULL, 80, 70, 40, 100, 60, 145, 495);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/425/215/199/618.png', 618, 'Stunfisk', 'GROUND', 'ELECTRIC', 109, 66, 84, 81, 99, 32, 471);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/426/215/199/619.png', 619, 'Mienfoo', 'FIGHTING', NULL, 45, 85, 50, 55, 50, 65, 350);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/427/215/199/620.png', 620, 'Mienshao', 'FIGHTING', NULL, 65, 125, 60, 95, 60, 105, 510);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/428/215/199/621.png', 621, 'Druddigon', 'DRAGON', NULL, 77, 120, 90, 60, 90, 48, 485);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/429/215/199/622.png', 622, 'Golett', 'GROUND', 'GHOST', 59, 74, 50, 35, 50, 35, 303);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/430/215/199/623.png', 623, 'Golurk', 'GROUND', 'GHOST', 89, 124, 80, 55, 80, 55, 483);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/431/215/199/624.png', 624, 'Pawniard', 'DARK', 'STEEL', 45, 85, 70, 40, 40, 60, 340);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/432/215/199/625.png', 625, 'Bisharp', 'DARK', 'STEEL', 65, 125, 100, 60, 70, 70, 490);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/433/215/199/626.png', 626, 'Bouffalant', 'NORMAL', NULL, 95, 110, 95, 40, 95, 55, 490);
INSERT INTO pokemon VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/434/215/199/627.png', 627, 'Rufflet', 'NORMAL', 'FLYING', 70, 83, 50, 37, 50, 60, 350);

INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/323/447/215/199/42901.png', 428, 'Mega Lopunny', 'NORMAL', 'FIGHTING', 65, 136, 94, 54, 96, 135, 580);
INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/323/434/215/199/10058.png', 445, 'Mega Garchomp', 'DRAGON', 'GROUND', 108, 170, 115, 120, 95, 92, 700);
INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/323/424/215/199/100076.png', 448, 'Mega Lucario', 'FIGHTING', 'STEEL', 70, 145, 88, 140, 70, 112, 625);
INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/323/435/215/199/10060.png', 460, 'Mega Abomasnow', 'GRASS', 'ICE', 90, 132, 105, 132, 105, 30, 594);
INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/503/215/199/635532020063317332.png', 475, 'Mega Gallade', 'PSYCHIC', 'FIGHTING', 68, 165, 95, 65, 115, 110, 618);
INSERT INTO mega VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/323/455/215/199/53201.png', 531, 'Mega Audino', 'NORMAL', 'FAIRY', 103, 60, 126, 80, 126, 50, 545);

INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/220/215/199/413.png', 1, 413, 'Wormadam (Sandy Cloak)', 'BUG', 'GROUND', 60, 79, 105, 59, 85, 36, 424);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/220/215/199/413.png', 2, 413, 'Wormadam (Trash Cloak)', 'BUG', 'STEEL', 60, 69, 95, 69, 95, 36, 424);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/228/215/199/421.png', 1, 421, 'Cherrim (Sunshine Form)', 'GRASS', NULL, 70, 60, 70, 87, 78, 85, 450);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', 1, 479, 'Rotom (Heat)', 'ELECTRIC', 'FIRE', 50, 65, 107, 105, 107, 86, 520);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', 2, 479, 'Rotom (Wash)', 'ELECTRIC', 'WATER', 50, 65, 107, 105, 107, 86, 520);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', 3, 479, 'Rotom (Frost)', 'ELECTRIC', 'ICE', 50, 65, 107, 105, 107, 86, 520);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', 4, 479, 'Rotom (Fan)', 'ELECTRIC', 'FLYING', 50, 65, 107, 105, 107, 86, 520);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/286/215/199/479.png', 5, 479, 'Rotom (Mow)', 'ELECTRIC', 'GRASS', 50, 65, 107, 105, 107, 86, 520);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/294/215/199/487.png', 1, 487, 'Giratina (Origin)', 'GHOST', 'DRAGON', 150, 120, 100, 120, 100, 90, 680);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/299/215/199/492.png', 1, 492, 'Shaymin (Sky Form)', 'GRASS', 'FLYING', 100, 103, 75, 120, 75, 127, 600);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/357/215/199/550.png', 1, 550, 'Basculin (Blue Form)', 'WATER', NULL, 70, 92, 65, 80, 55, 98, 460);
INSERT INTO form VALUES('https://media-cerulean.cursecdn.com/avatars/thumbnails/320/362/215/199/555.png', 1, 555, 'Darmanitan (Zen Mode)', 'FIRE', 'PSYCHIC', 105, 30, 105, 140, 105, 55, 540);

/* Views */
CREATE VIEW all_pokemons AS
    SELECT * FROM pokemon
    UNION
    SELECT * FROM mega
    UNION
    SELECT img_url, pokedex_entry, name, type1, type2, hp, atk, def, sat, sdf, spd, bst FROM form;

/* Procedures */
DELIMITER //
CREATE PROCEDURE Get_Pokemons ()
BEGIN
    SELECT * FROM all_pokemons;
END //

CREATE PROCEDURE Get_Pokemons_By_Type(IN type_p VARCHAR(20))
BEGIN
    SELECT * FROM all_pokemons
    WHERE type1 = type_p
    OR type2 = type_p;
END //

CREATE PROCEDURE Get_Pokemon(IN pokedex_entry_p INT)
BEGIN
    SELECT * FROM all_pokemons
    WHERE pokedex_entry = pokedex_entry_p;
END //