USE pokedex;

/* Bottom 10 Speed */
SELECT name
FROM pokemon
ORDER BY spd ASC
LIMIT 10;

/* Vulnerable to Ground and resistant to Steel */
SELECT p.name
FROM pokemon p
INNER JOIN resistance r 
ON p.type1 = r.resistant 
OR p.type2 = r.resistant 
WHERE r.weak = "STEEL" AND p.name IN (
    SELECT p.name
    FROM pokemon p
    INNER JOIN strength s
    ON p.type1 = s.vulnerable
    OR p.type2 = s.vulnerable
    WHERE s.strong = "GROUND"
);

/* BST between 200 and 500 and weak against Water */
SELECT p.name
FROM pokemon p
INNER JOIN resistance r
ON p.type1 = r.weak
OR p.type2 = r.weak
WHERE r.resistant = "WATER" AND p.bst >= 200 AND p.bst <= 500;

/* Highest Attack, has Mega and vulnerable to Fire */
SELECT p.name
FROM pokemon p
INNER JOIN strength s
ON p.type1 = s.vulnerable
OR p.type2 = s.vulnerable
INNER JOIN mega m
ON p.pokedex_entry = m.pokedex_entry
WHERE s.strong = "FIRE"
ORDER BY p.atk DESC
LIMIT 1;
