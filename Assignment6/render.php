<?php 
    require_once './vendor/autoload.php';
    require_once './pokemon_helper.php';
    require './router.php';
    $loader = new Twig_Loader_Filesystem('./templates');
    $twig = new Twig_Environment($loader);
    $template = $twig -> load('main.twig.html');

    echo $template->render(array(
        'title' => 'Pokedex',
        'popularPokemons' => $popularPokemons,
        'favoritePokemons' => $_SESSION['favoritePokemons'],
        'pokemonTable' => $pokemonTable
    ));
?>