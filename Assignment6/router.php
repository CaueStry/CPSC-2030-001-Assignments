<?php
    require_once 'pokemon_helper.php';

    function checkArray($array, $comparator) {
        foreach ($array as $i) {
            if ($i->entry == $comparator) {
                return true;
            }
        }
        return false;
    }

    session_start();
    if(!isset($_SESSION['favoritePokemons'])) {
        $_SESSION['favoritePokemons'] = array();
    }

    if(isset($_GET['addfavorite'])) {
        $index = array_search($_GET['addfavorite'], array_column($_SESSION['favoritePokemons'], 'entry'));
        if(checkArray($_SESSION['favoritePokemons'], $_GET['addfavorite'])) {
            array_splice($_SESSION['favoritePokemons'], $index, 1);
        } else if(sizeof($_SESSION['favoritePokemons']) < 7) {
            array_push($_SESSION['favoritePokemons'], getPokemon($_GET['addfavorite']));
        }
    }
?>