<?php
    require_once 'db.php';
    class Pokemon {
        function __construct($name, $img_url, $entry, $type1, $type2) {
            $this->name = $name;
            $this->img_url = $img_url;
            $this->entry = $entry;
            $this->type1 = $type1;
            $this->type2 = $type2;
        }
    }

    function getPokemon($id) {
        $result = getConnection()->query('CALL Get_Pokemon(\''.$id.'\')')->fetch_assoc();
        return new Pokemon(
            $result['name'],
            $result['img_url'],
            $result['pokedex_entry'],
            $result['type1'],
            $result['type2']
        );
    }

    function addToFavorite($pokemon) {
        array_push($_SESSION['favoritePokemons'], $pokemon);
    }

    $popularPokemons = array(
        getPokemon(448),
        getPokemon(445),
        getPokemon(491),
        getPokemon(493),
        getPokemon(483),
        getPokemon(492),
        getPokemon(487),
        getPokemon(475),
        getPokemon(471),
        getPokemon(466)
    );

    $pokemonTable = array();
    $pokemons = getConnection()->query('CALL Get_Pokemons()');
    while($pokemon = $pokemons->fetch_assoc()) {
        array_push($pokemonTable, $pokemon);
    }
?>