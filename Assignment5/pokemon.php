<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pokedex</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet/less" type="text/css" href="/less/pokemon.less" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
</head>

<body>
    <?php
        include 'insertion.php';
        $pokemon = getPokemon();
    ?>
    <main class="<?php echo strtolower($pokemon['type1']) ?>">
        <aside class="left">
            <div class="img-card <?php echo strtolower($pokemon['type1']) ?>">
                <img src="<?php echo $pokemon['img_url'] ?>">
            </div>
            <a href="/" class="back">Back to Pokedex</a>
        </aside>
        <aside class="right">
            <div class="info">
                <div class="name">
                    <?php echo $pokemon['name'] ?>
                </div>
                <div class="types">
                    <div class="type1 <?php echo strtolower($pokemon['type1']) ?>">
                        <?php echo $pokemon['type1'] ?>
                    </div>
                    <div class="type2 <?php echo strtolower($pokemon['type2']) ?>">
                        <?php echo $pokemon['type2'] ?>
                    </div>
                </div>
                <div class="stats">
                    <div class="stat">
                        <div class="label">HP:</div>
                        <div class="hp"><?php echo $pokemon['hp'] ?></div>
                    </div>
                    <div class="stat">
                        <div class="label">HP:</div>
                        <div class="atk"><?php echo $pokemon['atk'] ?></div>
                    </div>
                    <div class="stat">
                        <div class="label">DEF:</div>
                        <div class="def"><?php echo $pokemon['def'] ?></div>
                    </div>
                    <div class="stat">
                        <div class="label">SAT:</div>
                        <div class="sat"><?php echo $pokemon['sat'] ?></div>
                    </div>
                    <div class="stat">
                        <div class="label">SDF:</div>
                        <div class="sdf"><?php echo $pokemon['sdf'] ?></div>
                    </div>
                    <div class="stat">
                        <div class="label">SPD:</div>
                        <div class="spd"><?php echo $pokemon['spd'] ?></div>
                    </div>
                    <div class="stat">
                        <div class="label">BST:</div>
                        <div class="bst"><?php echo $pokemon['bst'] ?></div>
                    </div>
                </div>
            </div>
        </aside>
    </main>
</body>

</html>