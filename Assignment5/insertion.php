<?php
include 'db.php';

function getPokemons() {
    $conn = getConnection();
    if(isset($_GET['type'])) {
        $result = $conn->query('CALL Get_Pokemons_By_Type(\''.
        mysqli_real_escape_string($conn ,$_GET['type']).
        '\')');
    } else {
        $result = $conn->query('CALL Get_Pokemons');
    }
    return $result;
}


function insertPokemons($pokemons) {
    while($row = $pokemons->fetch_assoc()) {
        echo '<a href="/pokemon.php?id='.$row['pokedex_entry'].'" class="pokemon '.strtolower($row['type1']).'">'.
        '<img src="'.$row['img_url'].'">'.
        '<span class="entry">'.$row['pokedex_entry'].'</span>'.
        '<span class="name">'.$row['name'].'</span>'.
        '<span class="type1 '.strtolower($row['type1']).'">'.$row['type1'].'</span>'.
        '<span class="type2 '.strtolower($row['type2']).'">'.$row['type2'].'</span>'.
        '</a>';
    }
}

function getPokemon() {
    $conn = getConnection();
    $result = $conn->query('CALL Get_Pokemon(\''.
    mysqli_real_escape_string($conn ,$_GET['id']).
    '\')');
    return $result->fetch_assoc();
}
?>