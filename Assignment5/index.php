<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pokedex</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet/less" type="text/css" href="/less/index.less" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
</head>

<body>
    <header>
        <h1>Pokedex</h1>
        <div class="types">
            <a href="?" class="all">All</a>
            <a href="?type=normal" class="normal">Normal</a>
            <a href="?type=fighting" class="fighting">Fighting</a>
            <a href="?type=flying" class="flying">Flying</a>
            <a href="?type=poison" class="poison">Poison</a>
            <a href="?type=ground" class="ground">Ground</a>
            <a href="?type=rock" class="rock">Rock</a>
            <a href="?type=bug" class="bug">Bug</a>
            <a href="?type=ghost" class="ghost">Ghost</a>
            <a href="?type=steel" class="steel">Steel</a>
            <a href="?type=fire" class="fire">Fire</a>
            <a href="?type=water" class="water">Water</a>
            <a href="?type=grass" class="grass">Grass</a>
            <a href="?type=electric" class="electric">Electric</a>
            <a href="?type=psychic" class="psychic">Psychic</a>
            <a href="?type=ice" class="ice">Ice</a>
            <a href="?type=dragon" class="dragon">Dragon</a>
            <a href="?type=fairy" class="fairy">Fairy</a>
            <a href="?type=dark" class="dark">Dark</a>
        </div>
    </header>
    <main>
        <div class="pokemons">
            <?php 
                include 'insertion.php';
                insertPokemons(getPokemons());
            ?>
        </div>
    </main>
</body>

</html>